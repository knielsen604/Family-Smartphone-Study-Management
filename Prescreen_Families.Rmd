---
title: "Pre-Screening Survey Processing: Families"
author: "Kimberly Nielsen"
output: html_document
---

```{r echo=FALSE, warning=FALSE}
packages <- c("dplyr", "tidyr", "boxr", "RCurl", "calendar", "readxl", "bizdays", "tidyr", "psych", "stringr", "pracma")
install.packages(packages) # Install packages
lapply(packages, require, character.only=TRUE) # Load packages

knitr::opts_chunk$set(echo = TRUE) # Rmarkdown file requirement
box_auth() # Connect to the Box API
```

Upload and process the most recent Qualtrics pre-screening survey outputs.

```{r}
test <- list.files(path = "/Users/kimberlynielsen/Desktop/SFTP/Screenomics_Qualtrics_Exports") # Refresh the Qualtrics files automatically stored in the server
print(test) # Determine if there are any new survey responses from the specified date
```

If there are new survey responses from the specified date, continue to the next steps for data processing.

```{r}
day <- as.character(Sys.Date()-1) # Specify the date of pre-screening survey completion

qc_log <- grep(paste0("^fps_", day, sep=""), test, value=TRUE) # Teen enrollment questions, part 2
qc_log <- as.data.frame(read.csv(paste0("/Users/kimberlynielsen/Desktop/SFTP/Screenomics_Qualtrics_Exports/", qc_log, sep=""))) %>% .[.$Progress=="100",]
```

Filter out participants who are (1) eligible and (2) not ready to schedule an onboarding appointment OR unavailable during the listed times.

```{r}
# Participants who are not ready to schedule an OB appointment:
not_ready <- qc_log[qc_log$Q176=="I'm not ready to schedule a video conference now.",]
not_ready_yes <- not_ready[not_ready$Q186!="No" & (not_ready$Q52_7!="" | not_ready$Q52_8!=""),] 

to_delete <- not_ready$ResponseId[not_ready$Q186=="No"] # Participants who are eligible, not ready to schedule an appointment, and do not allow us to contact them, find and delete their record in Qualtrics.

# Participants for whom none of the OB appointment slots work:
no_times <- qc_log[qc_log$Q176=="None of these time slots works for me.",] %>%
            .[.$Q51_4!="" | .$Q51_5!="",] # Select participants who have provided contact information.

# Create a list of time slots that these participants may be available for: (mostly in case we decide to change time slots)
no_times_list <- as.data.frame(no_times$Q50)
names(no_times_list)[1] <- "time_slot_suggestion"

suggestions <- box_search("Time Slot Suggestions.csv", ancestor_folder_ids = "CENSORED") %>% box_read() %>% data.frame() # Read in "Time Slot Suggestions.csv"
no_times_list <- rbind.data.frame(no_times_list, suggestions)

#Combine both groups of participants:
no_ob_all <- rbind.data.frame(no_times, not_ready_yes)

if(nrow(no_times_list)>1){box_write(no_times_list, "Time Slot Suggestions.csv", dir_id="CENSORED")}

```

```{r}
# Select relevant columns and divide time-related columns into multiple columns:
 qc_eligible <- qc_log %>%
                        .[.$Q125=="No" | .$Q128=="No" | .$Q131=="No" | .$Q134=="No",] %>% #At least one adolescent is eligible, based on a "no" response to the last eligibility question
                        separate(., RecordedDate, into=c("Recorded Date", "Recorded Time"), sep=" ") %>%
                        separate(., Q176, into=c("Appointment Date", "Appointment Time"), sep=" @ ") %>%
                        separate(., `Appointment Time`, into=c("Appointment Time", "AMPM"), sep=" ") %>%
                        separate(., `Recorded Date`, into=c("Record Year", "Record Month", "Record Date"), sep="-")

# Wrangle and reformat the Qualtrics data:
qc_eligible <- as.data.frame(t(sapply(qc_eligible, function(i) gsub("${e://Field/", "", i, fixed=TRUE))))
qc_eligible[,which(colnames(qc_eligible)=="Weekday"):which(colnames(qc_eligible)=="AMPM")] <- sapply(qc_eligible[,which(colnames(qc_eligible)=="Weekday"):which(colnames(qc_eligible)=="AMPM")], function(i) gsub("}", "", i, fixed=TRUE))

# Assign each raw datetime field the correct label (e.g., input is "TODAYPLUS1" and output is "Monday, March 18th"):
qc_eligible$`Appointment Date` <- apply(qc_eligible, 1, function(x) { x[names(x)==x[names(x)=="Appointment Date"]] })
qc_eligible$`Appointment Time` <- apply(qc_eligible, 1, function(x) { unlist(x[names(x)==x[names(x)=="Appointment Time"]]) })
qc_eligible$`AMPM` <- apply(qc_eligible, 1, function(x) { unlist(x[names(x)==x[names(x)=="AMPM"]]) })
qc_eligible$`Appointment Time PST` <- str_replace(qc_eligible$`Appointment Time`, ":00", "") %>% as.numeric() #Remove ":00" from timestamps and convert time to numeric

# Convert all appointment times to PST (our time zone), depending on each participant's specified time zone. This is important for scheduling REDCap notifications and creating onboarding meeting events in Outlook:
for(i in 1:nrow(qc_eligible)){
  
# Convert the timestamps participants see (e.g., 3:00 p.m.) into standard time for ease in data wrangling:
  ifelse(qc_eligible$AMPM[i]=="PM" & qc_eligible$`Appointment Time PST`[i]!=12, 
         qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] + 12, 
         NA) #Change to 24-hour clock
  
# Add or subtract hours from the PST time, depending on participants' time zones:
  ifelse(qc_eligible$Q26[i]=="Hawaii-Aleutian Standard Time (HST)", qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] + 3, NA)
  ifelse(qc_eligible$Q26[i]=="Alaskan Standard Time (AKST)", qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] + 1, NA)
  ifelse(qc_eligible$Q26[i]=="Mountain Standard Time (MST)", qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] - 1, NA)
  ifelse(qc_eligible$Q26[i]=="Central Standard Time (CST)", qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] - 2, NA)
  ifelse(qc_eligible$Q26[i]=="Eastern Standard Time (EST)", qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] - 3, NA)
}

#Split up the appointment date into month and date:
qc_eligible <- separate(qc_eligible, `Appointment Date`, into=c("Weekday", "Month"), sep=", ") %>%
               separate(., Month, into=c("Month", "Date"), sep=" ")

# Wrangle dates and times so that they can be standardized later on:
qc_eligible$Date <- str_replace(qc_eligible$Date, "th", "") %>% str_replace(., "rd", "") %>% str_replace(., "st", "") %>% str_replace(., "nd", "") 
qc_eligible$Month <- sapply(qc_eligible$Month, function(i) gsub("January", 1, gsub("February", 2, gsub("March", 3, gsub("April", 4, gsub("May", 5, gsub("June", 6, gsub("July", 7, gsub("August", 8, gsub("September", 9, gsub("October", 10, gsub("November", 11, gsub("December", 12, i)))))))))))))

qc_eligible$`Record Year` <- as.numeric(qc_eligible$`Record Year`)
qc_eligible$`Record Month` <- as.numeric(qc_eligible$`Record Month`)

# If survey submission occurs in December and the appointment is in January, the appointment should be one year later than the year of pre-screening survey submission:
for(i in 1:nrow(qc_eligible)){ ifelse(qc_eligible$`Record Month`[i]==12 & qc_eligible$Month[i]==1, qc_eligible$`Record Year`[i] <- qc_eligible$`Record Year`[i]+1, NA)}

# Reformat dates for input into other documents:
qc_eligible$`Appt Date` <- paste(qc_eligible$`Record Year`, qc_eligible$Month, qc_eligible$Date, sep="-") #Y-M-D
qc_eligible$`Appointment Time PST` <- paste(qc_eligible$`Appointment Time PST`, ":00:00", sep="") #Put the ":00" back again and add milliseconds (after having removed it in an earlier step)
qc_eligible$`Time Slot` <- paste(qc_eligible$`Appt Date`, qc_eligible$`Appointment Time PST`, sep=" ") %>% 
                           sapply(., function(i) gsub(".*NA.*", NA, i)) 
qc_eligible$`Time Slot POSIX` <- as.POSIXct(qc_eligible$`Time Slot`, format="%Y-%m-%d %H:%M")

# Select participants who are ready to sign up for an OB appointment:
eligible_ready <- qc_eligible[is.na(qc_eligible$ZOOMLINK)==FALSE & qc_eligible$ZOOMLINK!="",] %>%
                  cbind.data.frame(., "FID"=NA) #Create new column for family IDs
```

Update eligible Family IDs, based on the number of families that (1) are eligible and (2) have signed up for an OB appointment.

```{r}
all_FID <- box_search("Total Family ID Numbers.csv", ancestor_folder_ids = "CENSORED") %>% box_read() %>% data.frame()

# Identify and subset rows with unused FIDs:
FID_available <- all_FID[all_FID$Used..1..or.Not.Used..0.==0,] #Select rows with unused FIDs
unused <- which(all_FID$Used..1..or.Not.Used..0.==0) #Identify row indices for unused FIDs

# Assign available FIDs to each family:
for(i in 1:nrow(eligible_ready)){ eligible_ready$FID[i] <- FID_available$FID[i] }

# Place 1's into FID sheet to indicate 'taken' status:
next_FID <- first(unused) + (nrow(eligible_ready)-1)
all_FID[first(unused):next_FID,]$Used..1..or.Not.Used..0. <- 1

box_write(all_FID, "Total Family ID Numbers.csv", dir_id="CENSORED")
```

Update individual family sheets.

```{r}
# Read in REDCap import templates. You can create one for your own dataset using the REDCap "Data Import Tool"; participant entries are listed in rows.
rc_template_teens <- box_search("Teen Import Template 1.csv") %>% box_read(., header=TRUE) %>% as.data.frame(.)
rc_template_adults <- box_search("Adult Import Template 2.csv") %>% box_read(., header=TRUE) %>% as.data.frame(.)
rc_template <- rc_template_teens[,c(1:15)] #These columns have the same format between parents and teens.

## Create individual FID sheet:
indiv_family_template <- box_search("Individual FID Template.csv") %>% box_read(., header=TRUE) %>% as.data.frame(.)
#indiv_family_template <- as.data.frame(read.csv(file.choose()))

cols_phone <-  c("Q105_4", "Q106_4", "Q107_4", "Q108_4", "Q109_4", "Q110_4", "Q111_4", "Q112_4") #Columns with parent and teen phone #s
cols_email <- c("Q216_4", "Q215_4", "Q214_4", "Q217_4", "Q109_5", "Q110_5", "Q111_5", "Q112_5") #Columns with parent and teen email addresses
parents <- c(1:4) %>% as.character(.) #Potential codes for parents
teens <- c(5:8) %>% as.character(.) #Potential codes for adolescents

#Create dataframes for for-loop in next step:
rc_parents_all <- data.frame()
rc_teens_all <- data.frame()
rc_ready <- data.frame()
```

Create calendar events, individual family ID sheets, and REDCap import templates for participants who (1) are eligible and (2) have scheduled an OB appointment.

```{r}
for(i in 1:nrow(eligible_ready)){

    indiv_raw <- eligible_ready[i,] # Select families one at a time
    indiv_2 <- qc_log[qc_log$ResponseId==indiv_raw$ResponseId,]
    indiv_raw$`Time Slot POSIX` <- as.POSIXct(indiv_raw$`Time Slot`, format="%Y-%m-%d %H:%M") #Convert time slot to POSIXct, or the 'time' format

    ## Create FID and REDCap imports: 
    indiv_FID <- cbind.data.frame(indiv_family_template , "PT"=c(replicate(4, "1"), replicate(4, "0"))) 
    
    indiv_FID$Family.ID.. <- as.character(indiv_raw$FID) #Assign the same family ID (FID) to all participants
    indiv_FID$Phone <- data.frame(t(indiv_raw)) %>% .[which(rownames(.) %in% cols_phone), 1] #Add adolescents' and parents' phone #s
    indiv_FID$Email <- data.frame(t(indiv_raw)) %>% .[which(rownames(.) %in% cols_email), 1] #Add adolescents' and parents' phone #s

 # When no participant contact information is present, simply create a blank FID sheet. All participant contact information and study IDs will be determined at the onboarding meeting:
 if(nrow(indiv_FID[indiv_FID$Phone!="" | indiv_FID$Email!="",])==0){
      
      indiv_FID_2 <- indiv_FID
      indiv_FID_2$Pre_Parents_Tested <- indiv_2$Q8
      indiv_FID_2$Pre_Teens_Tested <- indiv_2$Q23
      indiv_FID_2$Pre_Parents_Eligible <- nrow(indiv_FID[indiv_FID$PT==1 & (indiv_FID$Phone!="" | indiv_FID$Email!=""),])
      
      testing <- indiv_2[,c(55,64,73,82)] #Select final eligibility question for teens
      indiv_FID_2$Pre_Teens_Eligible <- rowSums(testing=="No") #Count how many teens are potentially eligible
      indiv_FID_2$Pre_Teens_Contact <- nrow(indiv_FID[indiv_FID$PT=="0" & (indiv_FID$Phone!="" | indiv_FID$Email!=""),]) # Count how many eligible teens provided contact information
      names(indiv_FID_2)[10] <- "How.hear.about.study"  
      
      box_write(list(indiv_FID, indiv_FID_2), paste("FID", indiv_FID$Family.ID..[1], "_", indiv_raw$`Appt Date`[1], ".xlsx", sep=""), dir_id="114558633558") # Create a family ID sheet for the family
  }

 # When at least one potential participant has listed contact information, create a more specialized FID sheet and a REDCap import:
 if(nrow(indiv_FID[indiv_FID$Phone!="" | indiv_FID$Email!="",])>0){

      # Create individual IDs for parents and teens:
      indiv_FID$Participant.ID..[indiv_FID$PT=="0" & (indiv_FID$Phone!="" | indiv_FID$Email!="")] <- paste(indiv_FID$Family.ID.., teens, sep="")
      indiv_FID$Participant.ID..[indiv_FID$PT=="1" & (indiv_FID$Phone!="" | indiv_FID$Email!="")] <- paste(indiv_FID$Family.ID.., parents, sep="")
      
      rc_ready <- indiv_FID[indiv_FID$Phone!="" | indiv_FID$Email!="",] # Select rows in FID with participant contact information
      
      indiv_FID_2 <- indiv_FID
      indiv_FID_2$Participant.ID.. <- NA
      indiv_FID_2$Email <- NA
      indiv_FID_2$Phone <- NA
      indiv_FID_2$Pre_Parents_Tested <- indiv_2$Q8
      indiv_FID_2$Pre_Teens_Tested <- indiv_2$Q23
      indiv_FID_2$Pre_Parents_Eligible <- nrow(indiv_FID[indiv_FID$PT==1 & (indiv_FID$Phone!="" | indiv_FID$Email!=""),]) # Number of eligible parents equals the number that provided contact information
     
      testing <- indiv_2[,c(55,64,73,82)] # Select final eligibility question for teens
      indiv_FID_2$Pre_Teens_Eligible <- rowSums(testing=="No") # Count how many teens are potentially eligible
      indiv_FID_2$Pre_Teens_Contact <- nrow(indiv_FID[indiv_FID$PT=="0" & (indiv_FID$Phone!="" | indiv_FID$Email!=""),]) # Count how many teens provided contact information in the form
      indiv_FID_2$Survey.type <- "Family"
    
      box_write(list(indiv_FID, indiv_FID_2), paste("FID", indiv_FID$Family.ID..[1], "_", indiv_raw$`Appt Date`[1], ".xlsx", sep=""), dir_id="114558633558") # Upload individual FID sheet for the family
      
# Create REDCap import entries for potential participants with contact information:
      rc_template_1 <- rc_template[1:nrow(rc_ready),] 

      rc_template_1$redcap_event_name <- "record_creation_arm_1"
      rc_template_1$recordid0101 <- rc_ready$Participant.ID..
      rc_template_1$ptparent0101 <- rc_ready$PT
      rc_template_1$tfamid0101 <- rc_ready$Family.ID..
      rc_template_1$tid0101 <- rc_ready$Participant.ID..
      rc_template_1$tphone0101 <- rc_ready$Phone
      rc_template_1$tem0101 <- rc_ready$Email
      rc_template_1$tqualtrics0101 <- indiv_raw$ResponseId

      rc_template_1$tpapt0101 <- eligible_ready$`Time Slot`[eligible_ready$FID %in% rc_ready$Family.ID..]
      rc_template_1$tpapt0102 <- eligible_ready$`Appt Date`[eligible_ready$FID %in% rc_ready$Family.ID..]
      rc_template_1$tpapt0103 <- eligible_ready$`Appointment Time`[eligible_ready$FID %in% rc_ready$Family.ID..]
      rc_template_1$tampm0101 <- eligible_ready$AMPM[eligible_ready$FID %in% rc_ready$Family.ID..] %>% sapply(., function(j) gsub("AM", "1", gsub("PM", "2", j)))
      rc_template_1$zoom0101 <- eligible_ready$ZOOMLINK[eligible_ready$FID %in% rc_ready$Family.ID..]

    all_adults <- rc_template_adults
    all_teens <- rc_template_teens

# Assign new rows to either the parent or teen REDCap import templates:
if(nrow(rc_template_1[rc_template_1$ptparent0101=="1",])>0){
    all_adults[1:nrow(rc_template_1[rc_template_1$ptparent0101=="1",]),1:14] <- rc_template_1[rc_template_1$ptparent0101=="1",]
    rc_parents_all <- rbind.data.frame(all_adults, rc_parents_all)}

if(nrow(rc_template_1[rc_template_1$ptparent0101=="0",])>0){
    all_teens[1:nrow(rc_template_1[rc_template_1$ptparent0101=="0",]),1:14] <- rc_template_1[rc_template_1$ptparent0101=="0",]
    rc_teens_all <- rbind.data.frame(all_teens, rc_teens_all)}
    }
  }
```

Create REDCap entries for participants who (1) are eligible, (2) have not scheduled an OB appointment, and (3) have provided some form of contact information. This allows us to send potential participants a link to the 'return later' survey on Qualtrics, where they can sign up for a different OB appointment.

```{r}
no_ob_all$Email <- paste(no_ob_all$Q51_5, no_ob_all$Q52_8, sep="")
no_ob_all$Phone <- paste(no_ob_all$Q51_4, no_ob_all$Q52_7, sep="")

# Create REDCap entries for participants who need a 'return later' survey:
if(nrow(no_ob_all)>0){
for(i in 1:nrow(no_ob_all)){
    
      indiv_raw <- no_ob_all[i,]
      rc_template_1 <- rc_template_adults[1,]
      
      rc_template_1$redcap_event_name[1] <- "record_creation_arm_1"
      rc_template_1$recordid0101[1] <- paste("unknown_", i, sep="")
      rc_template_1$tphone0101[1] <- indiv_raw$Phone[i]
      rc_template_1$tem0101[1] <- indiv_raw$Email[i]
      rc_template_1$tqualtrics0101[1] <- indiv_raw$ResponseId[i]
      rc_template_1$treturn0101[1] <- 1
      
rc_parents_all <- rbind.data.frame(rc_parents_all, rc_template_1)
  }
}

rc_teens_all <- rc_teens_all[,-ncol(rc_teens_all)] #Remove unnecessary 'X' column at the end of the dataframe
rc_parents_all <- rc_parents_all[,-ncol(rc_parents_all)]
rc_parents_all$tpapt0103[is.na(rc_parents_all$tpapt0103)==TRUE] <- NA

# Write new REDCap entries for parents and teens to Box:
if(nrow(rc_teens_all)>0){box_write(rc_teens_all, "RC_teens_all.csv", dir_id="136463104565")}
if(nrow(rc_parents_all)>0){box_write(rc_parents_all, "RC_parents_all.csv", dir_id="136463104565")}
```

Create Outlook calendar entries for new onboarding meetings.

```{r}
for(i in 1:nrow(eligible_ready)){

    indiv_raw <- eligible_ready[i,]
    indiv_raw$`Time Slot POSIX` <- as.POSIXct(indiv_raw$`Time Slot`, format="%Y-%m-%d %H:%M") + 60*60*7 #Convert time slot to POSIXct, or the 'time' format

    try <- ic_event(uid = ic_guid(),
           start_time = indiv_raw$`Time Slot POSIX`,
           end_time = 1,
           summary = paste("Onboarding Appointment (FID: ", eligible_ready$FID[i], "): ", as.character(eligible_ready$ZOOMLINK[i]), sep=""), more_properties = TRUE,
           event_properties = c("LOCATION"=as.character(eligible_ready$ZOOMLINK[i])))
  ic_write(try, file.path( paste("Family Onboarding Appointment (FID: ", indiv_raw$FID, ").ics", sep="")))
} 
```

