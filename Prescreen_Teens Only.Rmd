---
title: "Pre-Screening Survey Processing: Teens Only"
output: html_document
---

Install and/or load the necessary R packages. Connect to the Box API.

```{r echo=FALSE, warning=FALSE}
packages <- c("dplyr", "tidyr", "boxr", "RCurl", "calendar", "readxl", "bizdays", "tidyr", "psych", "stringr", "pracma")
install.packages(packages) # Install packages
lapply(packages, require, character.only=TRUE) # Load packages

knitr::opts_chunk$set(echo = TRUE) # Rmarkdown file requirement
box_auth() # Connect to the Box API

indiv_family_template <- box_search("Individual FID Template.csv") %>% box_read(., header=TRUE) %>% as.data.frame(.) # Create an individual FID sheet template
```

Upload and process the most recent Qualtrics pre-screening survey outputs.

```{r}
day <- as.character(Sys.Date()) # Specify the date of pre-screening survey completion

test <- list.files(path = "/Users/kimberlynielsen/Desktop/SFTP/Screenomics_Qualtrics_Exports") # Refresh the Qualtrics files automatically stored in the server
print(test) # Determine if there are any new survey responses from the specified date
```

If there are new survey responses from the specified date, continue to the next steps for data processing.

```{r}
# Download the latest version of the teen enrollment questions (part 2):
qc_log <- grep(paste0("^ten2_", day, sep=""), test, value=TRUE)
qc_log <- as.data.frame(read.csv(paste0("/Users/kimberlynielsen/Desktop/SFTP/Screenomics_Qualtrics_Exports/", qc_log, sep=""))) %>% 
                        .[.$Progress=="100",] 

# Split time-related columns into multiple columns:
qc_eligible <- qc_log %>%
                        separate(., RecordedDate, into=c("Recorded Date", "Recorded Time"), sep=" ") %>% 
                        separate(., Q176, into=c("Appointment Date", "Appointment Time"), sep=" @ ") %>%
                        separate(., `Appointment Time`, into=c("Appointment Time", "AMPM"), sep=" ") %>%
                        separate(., `Recorded Date`, into=c("Record Year", "Record Month", "Record Date"), sep="-")
  
# Wrangle and reformat the Qualtrics data:
qc_eligible <- as.data.frame(sapply(qc_eligible, function(i) gsub("${e://Field/", "", i, fixed=TRUE)))

qc_eligible[,which(colnames(qc_eligible)=="Appointment Date"):which(colnames(qc_eligible)=="AMPM")] <- sapply(qc_eligible[,which(colnames(qc_eligible)=="Appointment Date"):which(colnames(qc_eligible)=="AMPM")], function(i) gsub("}", "", i, fixed=TRUE))

# Assign each raw datetime field the correct label (e.g., input is "TODAYPLUS1" and output is "Monday, March 18th"):
qc_eligible$`Appointment Date` <- apply(qc_eligible, 1, function(x) { x[names(x)==x[names(x)=="Appointment Date"]] })
qc_eligible$`Appointment Time` <- apply(qc_eligible, 1, function(x) { unlist(x[names(x)==x[names(x)=="Appointment Time"]]) })
qc_eligible$`AMPM` <- apply(qc_eligible, 1, function(x) { unlist(x[names(x)==x[names(x)=="AMPM"]]) })
qc_eligible$`Appointment Time PST` <- str_replace(qc_eligible$`Appointment Time`, ":00", "") %>% as.numeric()

# Convert all appointment times to PST (Stanford University campus time zone). This is important for scheduling REDCap notifications and creating onboarding meeting events in Outlook:
for(i in 1:nrow(qc_eligible)){
  
  ifelse(qc_eligible$AMPM[i]=="PM" & qc_eligible$`Appointment Time PST`[i]!=12, qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] + 12, NA) #Change to 24-hour clock
  
# Add or subtract hours from the PST time, depending on participants' time zones:
  ifelse(qc_eligible$Q26[i]=="Hawaii-Aleutian Standard Time (HST)", qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] + 3, NA)
  ifelse(qc_eligible$Q26[i]=="Alaskan Standard Time (AKST)", qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] + 1, NA)
  ifelse(qc_eligible$Q26[i]=="Mountain Standard Time (MST)", qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] - 1, NA)
  ifelse(qc_eligible$Q26[i]=="Central Standard Time (CST)", qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] - 2, NA)
  ifelse(qc_eligible$Q26[i]=="Eastern Standard Time (EST)", qc_eligible$`Appointment Time PST`[i] <- qc_eligible$`Appointment Time PST`[i] - 3, NA)
}

# Split up the appointment date into month and date:
qc_eligible <- separate(qc_eligible, `Appointment Date`, into=c("Weekday", "Month"), sep=", ") %>%
               separate(., Month, into=c("Month", "Date"), sep=" ")

qc_eligible$Date <- str_replace(qc_eligible$Date, "th", "") %>% str_replace(., "rd", "") %>% str_replace(., "st", "") %>% str_replace(., "nd", "") #Wrangling the date column

qc_eligible$Month <- sapply(qc_eligible$Month, function(i) gsub("January", 1, gsub("February", 2, gsub("March", 3, gsub("April", 4, gsub("May", 5, gsub("June", 6, gsub("July", 7, gsub("August", 8, gsub("September", 9, gsub("October", 10, gsub("November", 11, gsub("December", 12, i)))))))))))))

qc_eligible$`Record Year` <- as.numeric(qc_eligible$`Record Year`)
qc_eligible$`Record Month` <- as.numeric(qc_eligible$`Record Month`)

# If survey submission occurs in December and the appointment is in January, the appointment should be one year later than the year of pre-screening survey submission.
for(i in 1:nrow(qc_eligible)){ ifelse(qc_eligible$`Record Month`[i]==12 & qc_eligible$Month[i]==1, 
                                        qc_eligible$`Record Year`[i] <- qc_eligible$`Record Year`[i]+1, 
                                        NA)}

# Reformat dates for input into other documents
qc_eligible$`Appt Date` <- paste(qc_eligible$`Record Year`, qc_eligible$Month, qc_eligible$Date, sep="-") #Y-M-D
qc_eligible$`Appointment Time PST` <- paste(qc_eligible$`Appointment Time PST`, ":00:00", sep="") #Put the ":00" back again and add milliseconds (after having removed it in an earlier step)
qc_eligible$`Time Slot` <- paste(qc_eligible$`Appt Date`, qc_eligible$`Appointment Time PST`, sep=" ") %>% #Create an OB appointment 'time slot' by combining date and time variables
                           sapply(., function(i) gsub(".*NA.*", NA, i))
qc_eligible$`Time Slot POSIX` <- as.POSIXct(qc_eligible$`Time Slot`, format="%Y-%m-%d %H:%M")

# Select participants who are ready to sign up for an OB appointment:
eligible_ready <- qc_eligible[is.na(qc_eligible$ZOOMLINK)==FALSE & qc_eligible$ZOOMLINK!="",] %>%
                  cbind.data.frame(., "FID"=NA) #Create new column for family IDs
```

Update eligible Family IDs, based on the number of families that (1) are eligible and (2) have signed up for an OB appointment.

```{r}
all_FID <- box_search("Total Teen ID Numbers.csv") %>% box_read() %>% data.frame() # Download the ongoing list of teens-only FID numbers

# Identify and subset rows with unused FIDs:
FID_available <- all_FID[all_FID$Used..1..or.Not.Used..0.==0,] #Select rows with unused FIDs
unused <- which(all_FID$Used..1..or.Not.Used..0.==0) #Identify row indices for unused FIDs

# Assign available FIDs to each family:
for(i in 1:nrow(eligible_ready)){ eligible_ready$FID[i] <- FID_available$FID[i] }

# Place 1's into FID sheet to indicate 'taken' status:
next_FID <- first(unused) + (nrow(eligible_ready)-1)
all_FID[first(unused):next_FID,]$Used..1..or.Not.Used..0. <- 1

box_write(all_FID, "Total Teen ID Numbers.csv", dir_id="CENSORED")
```

Create calendar events and individual family ID sheets for participants who have scheduled an OB appointment.

```{r}
for(i in 1:nrow(eligible_ready)){

    indiv_raw <- eligible_ready[i,]
    indiv_raw$`Time Slot POSIX` <- as.POSIXct(indiv_raw$`Time Slot`, format="%Y-%m-%d %H:%M") + 60*60*7 #Convert time slot to POSIXct, or the 'time' format

   # Write a calendar event that can later be imported into Outlook Calendar:
    try <- ic_event(uid = ic_guid(),
           start_time = indiv_raw$`Time Slot POSIX`,
           end_time = 1,
           summary = paste("Onboarding Appointment (FID: ", eligible_ready$FID[i], "): ", as.character(eligible_ready$ZOOMLINK[i]), sep=""), more_properties = TRUE,
           event_properties = c("LOCATION"=as.character(eligible_ready$ZOOMLINK[i])))
    ic_write(try, file.path( paste("Family Onboarding Appointment (FID: ", indiv_raw$FID, ").ics", sep="")))
  
    ## Create FID and REDCap imports: 
    indiv_FID <- cbind.data.frame(indiv_family_template , "PT"=c(replicate(4, "1"), replicate(4, "0"))) # Dictates whether participant is a parent or teen 
    indiv_FID$Family.ID.. <- as.character(indiv_raw$FID) #Assign the same family ID (FID) to all participants
    indiv_FID$Pre_Parents_Tested <- 0 # Parents aren't tested on teen pre-screening survey
    indiv_FID$Pre_Teens_Tested <- 1 # Only one teen can be tested
    indiv_FID$Pre_Parents_Eligible <- 0
    indiv_FID$Pre_Teens_Eligible <- 1
    indiv_FID$Pre_Teens_Contact <- 0 # Teens cannot specify their contact information prior to onboarding meetings 
    indiv_FID$Survey.type <- "Teens"
      
    box_write(indiv_FID, paste("FID", indiv_FID$Family.ID..[1], "_", indiv_raw$`Appt Date`[1], ".xlsx", sep=""), dir_id="CENSORED") # Create individual FID sheet to edit after the onboarding meeting
}
```




